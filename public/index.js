const socketServer = config.socketServer || "https://localhost:8080";
let debug = false;
let socket
let this_client_id;
let el = {};
let clients = {};
let consoleElement;

window.onload = async () => {
    el.form = document.querySelector("#form");
    el.prompt1 = document.querySelector("#prompt1");
    el.prompt2 = document.querySelector("#prompt2");
    el.submit_button = document.querySelector("#submit_button");
    el.processing = document.querySelector("#processing_container");
    setupConsole();
    initSocketConnection();
    setupForm();
};


const isEmpty = str => !str.trim().length;

const setupForm = () => {

    el.form.addEventListener("input", (e) => {
        if (isEmpty(el.prompt1.value) || isEmpty(el.prompt2.value)) {
            disableButton();
        } else {
            enableButton();
        }
    });


    el.submit_button.addEventListener("click", (e) => {
        e.preventDefault();
        sendPrompts();
        showProcessing();
        setTimeout(hideProcessing, 30000);
        clearForm();
    });
}

const enableButton = () => {
    el.submit_button.disabled = false;
    el.submit_button.classList.remove('btn-secondary');
    el.submit_button.classList.add('btn-primary');
};

const disableButton = () => {
    el.submit_button.disabled = true;
    el.submit_button.classList.add('btn-secondary');
    el.submit_button.classList.remove('btn-primary');
}


const addToConsole = (_string) => {
    el.console.innerHTML += "<br>" + _string;
}

const sendPrompts = () => {
    socket.emit("message", {
        message: "lerp",
        prompt1: el.prompt1.value,
        prompt2: el.prompt2.value,
        id: this_client_id,
    });
}

const showProcessing = () => {
    el.processing.style.display = 'block';
};

const hideProcessing = () => {
    el.processing.style.display = 'none';
}

const clearForm = () => {
    el.prompt1.value = "";
    el.prompt2.value = "";
    disableButton();
}

const setupConsole = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    if (params.debug) {
        debug = true;
    }
    if (debug && "console" in window) {
        methods = [
            "log", "assert", "clear", "count",
            "debug", "dir", "dirxml", "error",
            "exception", "group", "groupCollapsed",
            "groupEnd", "info", "profile", "profileEnd",
            "table", "time", "timeEnd", "timeStamp",
            "trace", "warn"
        ];

        generateNewMethod = function (oldCallback, methodName) {
            return function () {
                var args;
                addToConsole(methodName + ":" + arguments[0]);
                args = Array.prototype.slice.call(arguments, 0);
                Function.prototype.apply.call(oldCallback, console, arguments);
            };
        };

        for (i = 0, j = methods.length; i < j; i++) {
            cur = methods[i];
            if (cur in console) {
                old = console[cur];
                console[cur] = generateNewMethod(old, cur);
            }
        }

    }
}

function initSocketConnection() {
    console.log("attempting socket connection");
    socket = io(socketServer, { secure: true });
    socket.on("connect", () => {
        console.log("socket.io connected to " + socketServer);
    });

    socket.on("connect_failed", (e) => {
        console.log("connect_failed");
    });

    socket.on("error", (e) => {
        console.log("error: " + e);
    });

    socket.on("introduction", (payload) => {
        this_client_id = payload.id;
        this_client_index = payload.client_index;
        console.log("introduced as client_index" + payload.client_index);
    });
}
